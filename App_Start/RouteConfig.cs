﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebAPIRecruit
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}",
            defaults: new { controller = "Home", action = "Index" }
        );
            routes.MapRoute(
                name: "GetTech",
                url: "{controller}/{action}",
                defaults: new { action = "GetAllTechnologies", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "GetCand",
                url: "{controller}/{action}",
                defaults: new { action = "GetAllAcceptedCandidates", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Action",
                url: "{controller}/{action}/{Technology}/{Experience}",
                defaults: new { action = "GetCandidates", Technology = UrlParameter.Optional, Experience = UrlParameter.Optional }
                );
            routes.MapRoute(
                name: "Candidate",
                url: "{controller}/{action}/{CandidateID}/{Accept}/{Name}",
                defaults: new { action = "PutAcceptReject", ID = UrlParameter.Optional, Accept = UrlParameter.Optional, Name = UrlParameter.Optional }
                );
        }
    }
}
