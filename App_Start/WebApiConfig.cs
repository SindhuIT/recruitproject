﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebAPIRecruit
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}",
                defaults: new { controller = "Home", action = "Index" }
            );
            config.Routes.MapHttpRoute(
                name: "GetTechApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { action = "GetAllTechnologies", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "GetCandApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { action = "GetAllAcceptedCandidates", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{Technology}/{Experience}",
                defaults: new { action = "GetCandidates", Technology = RouteParameter.Optional, Experience = RouteParameter.Optional }
                );
            config.Routes.MapHttpRoute(
                name: "CandidateApi",
                routeTemplate: "api/{controller}/{action}/{CandidateID}/{Accept}/{Name}",
                defaults: new { action = "PutAcceptReject", CandidateID = RouteParameter.Optional, Accept = RouteParameter.Optional, Name = RouteParameter.Optional }
                );
        }
    }
}
