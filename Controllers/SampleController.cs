﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using WebAPISampleSS.Models;

namespace WebAPISampleSS.Controllers
{
    public class SampleController : ApiController
    {
        //Technologies from the link
        public List<TechnologyModel> resultTechnology {
            get 
            {
                string reqTechnology = webRequestResponse("https://v1.ifs.aero/technologies/"); 
                return JsonConvert.DeserializeObject<List<TechnologyModel>>(reqTechnology);
            }
            set 
            { 
                resultTechnology = value; 
            }
        }
        //Candidates from the link
        public List<CandidateModel> resultCandidate
        {
            get
            {
                string reqCandidate = webRequestResponse("https://v1.ifs.aero/candidates/");
                return JsonConvert.DeserializeObject<List<CandidateModel>>(reqCandidate);
            }
            set
            {
                resultCandidate = value;
            }
        }
        //Response from the links
        public string webRequestResponse(string url)
        {
            string response = "";
            HttpWebRequest reqWebrequest = (HttpWebRequest)WebRequest.Create(url);
            WebResponse respRequest = reqWebrequest.GetResponse();
            StreamReader reader = new StreamReader(respRequest.GetResponseStream());
            response = reader.ReadToEnd();
            return response;
        }
        //Get the matching candidates
        public IHttpActionResult GetCandidates(string Technology, int Experience)
        {
            if (resultCandidate != null)
            {
                //Filter Technology with experience
                var matchingCandidates = resultCandidate.Where(c => c.technologies.Any(d => d.name.ToLowerInvariant() == Technology.ToLowerInvariant() && d.experianceYears >= Experience));
                var jsonResult = JsonConvert.SerializeObject(matchingCandidates);
                return Ok(jsonResult);
            }
            return Ok();
        }
        //Get all the Technologies
        public IHttpActionResult GetAllTechnologies()
        {
            //All Technologies 
            var jsonResult = JsonConvert.SerializeObject(resultTechnology);
            return Ok(jsonResult);
        }
        //Get all the accepted candidates
        public List<CandidateModel> GetAllAcceptedCandidates()
        {
            string procedureName = "[dbo].[sp_GetAllAcceptedCandidates]";
            var result = new List<CandidateModel>();
            string sqlConnect = ConfigurationManager.ConnectionStrings["sqlconnectinstring"].ConnectionString; 
            using (SqlConnection connection = new SqlConnection(sqlConnect))
            {
                using (SqlCommand command = new SqlCommand(procedureName, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = int.Parse(reader[0].ToString());
                            Guid CandidateID = Guid.Parse(reader[1].ToString());
                            string CandidateName = reader[2].ToString();
                            string Email = reader[3].ToString();
                            string Phone = reader[4]?.ToString();
                            bool Accepted = Convert.ToBoolean(reader[5]);

                            CandidateModel tmpRecord = new CandidateModel()
                            {
                                id = id,
                                candidateName = CandidateName,
                                email = Email,
                                phone = Phone,
                                accepted = Accepted
                            };
                            result.Add(tmpRecord);
                        }
                    }
                }
            }
            return result;
        }
        [HttpGet]
        //Insert/update the candidates in DB
        public IHttpActionResult PutAcceptReject(Guid CandidateID, string Accept,string Name)
        {
            if (Accept == "true")
            {
                CandidateModel candidateResults = resultCandidate.Where(C => C.guid == CandidateID).FirstOrDefault();
                //Procedure call to Insert Accepted the candidate details to candidate Table and 
                //Experience to TechnologywithExperience table.
            }
            else
            {
                //Procedure call to Insert/update the rejection of the candidate
            }
            return Ok();
        }
    }
}