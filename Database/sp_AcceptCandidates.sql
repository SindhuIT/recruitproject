USE [RecruitCandidates]
GO
/****** Object:  StoredProcedure [dbo].[sp_AcceptCandidates]    Script Date: 1/25/2020 12:46:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_AcceptCandidates] 
	-- Add the parameters for the stored procedure here
	@CandidateID UNIQUEIDENTIFIER ,
    @CandidateName NVARCHAR,
    @Email NVARCHAR,
	@Phone NVARCHAR,
	@Accepted BIT
AS
BEGIN
	DECLARE @IDTEMP INT 
	 SELECT @IDTEMP  = COUNT(ID)+1 FROM Candidates

INSERT INTO [dbo].[Candidates]
           ([ID]
           ,[CandidateID]
           ,[CandidateName]
           ,[Email]
           ,[Phone]
           ,[Accepted])
     VALUES
           (@IDTEMP
           ,@CandidateID
           ,@CandidateName
           ,@Email
           ,@Phone
           ,@Accepted)
--Create a table for technology with experience and insert the details 
END
