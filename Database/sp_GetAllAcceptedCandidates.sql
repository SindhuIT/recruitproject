USE [RecruitCandidates]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllAcceptedCandidates]    Script Date: 1/25/2020 12:43:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAllAcceptedCandidates] 
	AS
BEGIN
SELECT [ID]
      ,[CandidateID]
      ,[CandidateName]
      ,[Email]
      ,[Phone]
      ,[Accepted]
  FROM [dbo].[Candidates] WHERE Accepted=1
END
GO

