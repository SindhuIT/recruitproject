﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPISampleSS.Models
{
    public class CandidateModel
    {
        public name name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public List<technologies> technologies { get; set; }

        public Guid guid;
        public bool accepted { get; set; }
        public int id { get; set; }
        public string candidateName { get; set; }
    }

    public class name
    {
        public string first { get; set; }
        public string last { get; set; }
    }
    public class technologies{   //TechnologyWithExperience
        public int experianceYears { get; set; }
        public string name { get; set; }

    }

}